using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dio_payment_api.Dto.Vendedor;

namespace dio_payment_api.Models
{
    public class Vendedor
    {
        public int Id { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        public Vendedor()
        {

        }

        public Vendedor(CadastrarVendedorDTO dto)
        {
            Cpf = dto.Cpf;
            Nome = dto.Nome;
            Email = dto.Email;
            Telefone = dto.Telefone;
        }
    }
}