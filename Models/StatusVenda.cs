using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dio_payment_api.Models
{
    public enum StatusVenda
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}