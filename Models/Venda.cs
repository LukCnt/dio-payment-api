using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dio_payment_api.Dto.Venda;
using dio_payment_api.Dto.Vendedor;

namespace dio_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public int VendedorId  { get; set; }
        public Vendedor Vendedor { get; set; }
        public string Itens { get; set; }
        public StatusVenda Status { get; set; }

        public Venda()
        {

        }

        public Venda(CadastrarVendaDTO dto)
        {
            Data = dto.Data;
            VendedorId = dto.VendedorId;
            Itens = dto.Itens;
            Status = dto.Status;
        }
    }
}