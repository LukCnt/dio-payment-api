using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dio_payment_api.Dto.Vendedor;
using dio_payment_api.Models;
using dio_payment_api.Repository;
using Microsoft.AspNetCore.Mvc;

namespace dio_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly VendedorRepository _repository;

        public VendedorController(VendedorRepository repository)
        {
            _repository = repository;
        }

        [HttpPost("CadastrarVendedor")]
        public IActionResult Cadastrar(CadastrarVendedorDTO dto)
        {
            var vendedor = new Vendedor(dto);
            _repository.Cadastrar(vendedor);
            return Ok(vendedor);
        }

        [HttpGet("ListarVendedores")]
        public IActionResult ListarVendedores()
        {
            var vendedores = _repository.ListarVendedores();
            return Ok(vendedores);
        }        
    }
}