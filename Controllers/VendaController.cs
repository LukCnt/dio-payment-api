using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dio_payment_api.Dto.Venda;
using dio_payment_api.Models;
using dio_payment_api.Repository;
using Microsoft.AspNetCore.Mvc;

namespace dio_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaRepository _repository;

        public VendaController(VendaRepository repository)
        {
            _repository = repository;
        }

        [HttpPost("CadastrarVenda")]
        public IActionResult Cadastrar(CadastrarVendaDTO dto)
        {
            var venda = new Venda(dto);
            _repository.Cadastrar(venda);
            return Ok(venda);
        }

        [HttpGet("BuscarVendaPorId/{id}")]
        public IActionResult BuscarPorId(int id)
        {
            var venda = _repository.BuscarPorId(id);

            if (venda is not null)
            {
                return Ok(venda);
            }
            else
            {
                return NotFound(new{Mensagem="Venda não encontrada!"});
            }
        }
    }
}