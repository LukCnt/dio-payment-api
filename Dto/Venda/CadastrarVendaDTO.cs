using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dio_payment_api.Models;

namespace dio_payment_api.Dto.Venda
{
    public class CadastrarVendaDTO
    {
        public DateTime Data { get; set; }
        public int VendedorId  { get; set; }
        public string Itens { get; set; }
        public StatusVenda Status { get; set; }
    }
}