using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dio_payment_api.Dto.Vendedor
{
    public class CadastrarVendedorDTO
    {
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}