using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dio_payment_api.Context;
using dio_payment_api.Models;

namespace dio_payment_api.Repository
{
    public class VendedorRepository
    {
        private readonly VendasContext _context;

        public VendedorRepository(VendasContext context)
        {
            _context = context;
        }

        public void Cadastrar(Vendedor vendedor)
        {
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
        }

        public List<Vendedor> ListarVendedores()
        {
            var vendedores = _context.Vendedores.ToList();

            return vendedores;
        }
    }
}