using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dio_payment_api.Context;
using dio_payment_api.Models;
using Microsoft.EntityFrameworkCore;

namespace dio_payment_api.Repository
{
    public class VendaRepository
    {
        private readonly VendasContext _context;

        public VendaRepository(VendasContext context)
        {
            _context = context;
        }

        public Venda Cadastrar(Venda venda)
        {
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return venda;
        }

        public Venda BuscarPorId(int id)
        {
            var venda = _context.Vendas.Include(x => x.Vendedor)
                                       .FirstOrDefault(x => x.Id == id);

            return venda;
        }
    }
}